#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
	echo "Please run this script as root"
	exit 1;
fi

SEP_THIN="-------------------------------------------------------------------"
SEP_THICK="==================================================================="

CURRENT_DIR=$( pwd )
HOME_DIR='/home/'$( eval echo $SUDO_USER )

echo "Home = $HOME_DIR | Current Directory = $CURRENT_DIR" 
echo "Installing dependencies -> perl"

sudo apt-get install perl

printf "
$SEP_THICK\n
Please make sure that you have the following before continuing\n
$SEP_THIN\n
1. Your Ellucian artifactory credentials\n
2. BANDEV Access and credentials\n
3. SSH access to git@gitvc.tru.ca server - If you don't have this, cancel this script then enter the command ssh-keygen to generate private and public keys. Then copy the file ~/.ssh/id_rsa.pub to the desktop and send it to Rick Gunnyon asking to be added to the git server
\n$SEP_THIN\n
Continue? (Y/n)
";

read CHOICE;

if [[ $CHOICE == "n" ]] || [[ $CHOICE == "N" ]]; then
	echo "User interrupt, exiting";
	exit 1;
fi

printf "If you are importing ssh keys from another machine, place them in the ~/Desktop/PUT_SSH_KEYS_HERE folder and hit enter. \n\nIf you have generated the ssh key using ssh-keygen, just hit enter\n\n$SEP_THIN\n";
 
read GRBG

if [[ $( ls ~/Desktop/PUT_SSH_KEYS_HERE | wc -l ) == 0 ]] && [[ $( ls ~/.ssh | wc -l ) == 0 ]]; then
	printf "No keys have been detected in the PUT_SSH_KEYS_HERE directory.\n\nNo keys have been detected in the ~/.ssh directory\n\nPlease either run ssh-keygen or place key files in the proper directories\n"
	exit 1;
else
	echo "Copying ssh keys into ~/.ssh folder";
	sudo cp $HOME_DIR/Desktop/PUT_SSH_KEYS_HERE/* $HOME_DIR/.ssh/
fi

echo $SEP_THIN

echo "Editing settings.xml, please enter your artifactory credentials.";

printf "Artifactory Username: ";
read ARTIFACTORY_USER

printf "Artifactory Password: ";
read ARTIFACTORY_PASS

perl -pi -e "s/artifactory_user/$ARTIFACTORY_USER/g" $HOME_DIR/.m2/settings.xml

perl -pi -e "s/artifactory_pass/$ARTIFACTORY_PASS/g" $HOME_DIR/.m2/settings.xml

echo "Editing /build/webapp-services/src/main/resources/config.properties, please enter your BANDEV credentials";

printf "BANDEV Username (probably dv_): "
read BANDEV_USER

printf "BANDEV Pass: ";
read BANDEV_PASS

perl -pi -e "s/bandev_user/$BANDEV_USER/g" $USER_HOME/admin_page/dev90/build/webapp-services/src/main/resources/config.properties

perl -pi -e "s/bandev_pass/$BANDEV_PASS/g" $USER_HOME/admin_page/dev90/build/webapp-services/src/main/resources/config.properties

printf "\n\n$SEP_THICK\n\nSuccess! Your development environment has been configured.\n\n$SEP_THICK\n\n";




















