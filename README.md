# Troubleshooting Document for Using eclipse with Banner 9 Forms

Note: This document works as a supplement to the official Ellucian Documentation: Banner Transformed: Working with Eclipse and Your Administrative Application. All these commands are relevant for the **Linux** operating system. They might work for Windows 'Git Bash' but haven't been tested.

### Some important links
- [Morphis frames documentation](https://confluence.morphis-tech.com/display/FRAMES/Developer+Guide)
- [Eclipse Oxygen](https://www.eclipse.org/oxygen/)
- [oracle's archive download page](http://www.oracle.com/technetwork/ja    va/javase/downloads/java-archive-downloads-javase7-521261.html)


## How to edit this document

This document is written in [Markdown](https://en.wikipedia.org/wiki/Markdown), a very simple, intuitive markup language designed to be easily read both rendered and un-rendered. 

An excellent markdown cheat sheet can be found [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

### Contributing files to the documentation

Simply clone this repository onto your computer. To do so you either need [git bash (for windows)](https://git-scm.com/downloads) or the `git` program for linux. The linux variant can be installed by entering `sudo apt-get install git` into a terminal.

To clone the repository open git bash if you are using windows or terminal if you are using linux and enter the command:

`git clone https://bitbucket.org/kyle_behiels/banner9notes`

Once you have this repository cloned you can make changes to it whenever you want. 

**If you want to update this document in the future, run the following in the command line before making changes.**

`cd ~/path/to/repo/banner9forms && git pull`

### Configuring git & getting a bitbucket account 

If this is your first time using git you will need to configure it with the following two commands.

`git config --global user.name "Your Name"`

`git config --global user.email "Your Email"`

Then navigate over to [https://bitbucket.org](Bitbucket) and create an account with the same email that you just used. Username does not matter.

Once this has been done you should now be able to edit the documentation and commit changes.

To edit the custom documentation, open the file README.md and go nuts. Other files can be added (things like scripts and supporting pdf's) by simply adding them to the banner9forms folder.

Once you are happy with your changes, run the following set of commands:

cd to the banner9forms directory

`cd ~/path/to/repo/banner9forms`

Stash your changes

`git stash`

Ensure that you are up to date with everyone else 

`git pull`

Re-apply your changes

`git stash apply`

Stage your changes - **Please write a good commit message.** It helps people understand why you are making these changes.

`git add --all && git commit -m "Your thoughtful, insightful and helpful commit message."`

Push your changes to the git server - You will be prompted to log in to bit bucket. If you are on windows you might even get a pretty GUI. Log in with your bitbucket **username** (email works too) and password.

`git push origin master`

All done, your changes should now be visible online.

___
## Index - Get environment set up

- Using the Linux Mint pre-built machine
- Installing a custom virtual machine
- Git and version control overview
- Git clone commands
- Importing projects into Eclipse
- Removing Rick's config Information
- Javascript Version Issue
  - Installing Java 7
  - Set Java 7 as system default
  - Configure your projects to use Java 7
- Common Libraries dependency Issue
- Fixing dependencies
- Adding pre-existing key to new install
___

## Index - Development

- Creating a form with a keyblock

___

## Using the Linux Mint pre-built machine

Download and install [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Download the virtual machine from `O:\IT Services Share\EnterpriseSystems\Banner 9 Custom Forms\Virtual Machines\Linux Mint.rar` and extract it. (requires [Winrar](https://www.win-rar.com/download.html?&L=0))

Open Oracle VirtualBox and click `Machines > Add` and browse to the `Linux Mint > Linux Mint.vbox` file and click `Ok` 

The virtual machine should now appear on the left menu of the VirtualBox GUI. Click on it to highlight it and click `Settings` 

In settings make the following changes;

- General > Advanced > Shared Clipboard: **Bidirectional**
- General > Advanced > Drag'n'drop: **Bidirectional**
- System > Motherboard > Base Memory: **Between 10 and 14 Gb (10000Mb -> 14000Mb)** 
- System > Processor > Processor(s): **8 CPU Processor -> 6 , 4 CPU Processor -> 3**
- Display > Screen > Video Memory: **128 MB**

Click `ok` and then `Start` to boot the virtual machine

**Default Username**: Linda

**Default Password**: trudevpass

**Default Keyring Password**: lindatemp

To save time and headache, a Linux VM for use with Oracle Virtualbox has been created that can be used by anyone.

The default username is "Linda" because that is who the VM was originally created for and due to the name being included in most file path's for almost all config files, I decided changing it was not worth the possible complications. If this really bugs you, do the following to change the account name though I would recommend just leaving it. Note that this will not change the name of the "Home" directory.

`ctrl + alt + f5` to open a seperate TTY shell then type

`sudo usermod -l <NEW NAME> Linda` 

### Setting up the virtual machine

The script on the desktop called "initial_setup.sh" should make this process fairly easy. To use the script make sure that you have the following

- A SSH private and public key pair. The SSH key will need to be added to the repository by Rick Gunnyon
- A dv_ account with BANDEV access
- Ellucian Artifactory credentials 

If you have these three accounts, skip the next section on how to get them.

### Getting the accounts

**SSH Keys for the git server**

Open up a terminal with `ctrl + alt + t` and type `ssh-keygen` accept all of the defaults by pressing enter (Leave the password blank)

Run the command `sudo cp ~/.ssh/id_rsa.pu ~/Desktop/<YOURNAME>.pub` Replacing `<YOURNAME>` with your actual name, no uppercase and no spaces.

Close the terminal and look on your desktop. If everything went well you should see the file `<YOURNAME>.pub`. Send this file to Rick Gunnyon and tell him that you would like access to the server git@gitvc.tru.ca for banner9 form development


**DV account with BANDEV Access**

Karl and Austin both have the authority to issue this account. Tell either of them that you need the direct access (NOT SSO) for Banner9 forms development. 

**Ellucian Artifactory Credentials**

You do not actually need this account to work on the forms but if at any point an update is issued to an ellucian plugin, you will need this to get the update.

Start by creating an Ellucian Hub account [here](https://clientapps.ellucian.com/SignUp)

When you have created that account, log in and click `Ellucian Support Center`. In the support center create a case and title it `Request for Artifactory Credentials`, leaving the body field blank. After a couple of days an Ellucian tech will provide you with Artifactory credentials

### Configuring your setup 

Configuration is done with the `initial_setup.sh` script. Open a terminal with `ctrl + alt + t` and type `sudo ./Desktop/initial_setup.sh` and follow the prompts.

If you are migrating from another virtual machine with old SSH keys, copy the entire contents of the `~/.ssh` folder on the old computer to the `~/Desktop/PUT_SSH_KEYS_HERE` on the Virtual Machine. 

If you did the `ssh-keygen` on this computer simply hit enter until the script prompts you to enter your information. If the script is successful, your environment is configured and ready for development! 

---
## Installing a custom virtual machine

There are hundreds of linux distributions to choose from but this documentation is written to be relevant on all Debian based distributions.

Here are some that you can choose from:

- [Ubuntu](https://www.ubuntu.com/) (recommended) - Comes pre installed with gnome desktop (might be a little weird for windows users). Has some of the most extensive documentation.
  - [Kubuntu](https://kubuntu.org/getkubuntu/) - Same as ubuntu but comes with KDE desktop (more intuitive for windows users).
  - [Lubuntu](https://lubuntu.net/downloads/) - Same as ubuntu but comes with the XFCE desktop. Very lightweight (Light Ubuntu -> Lubuntu) so good for low power computers
  - [Ubuntu Mate](https://ubuntu-mate.org/) - Same as ubuntu but comes with the MATE desktop. The MATE desktop is by far the most similar to windows and can make for an easier transition.
- [Debian](https://www.debian.org/distrib/netinst) - Debian is the minimalist Distro. Considered one of the most stable distros in existence.
- [Elementary OS](https://elementary.io/) - Based on Ubuntu and touted as the MacOS replacement. Very simple but kind of locked down at times.
  - Elementary OS is free, on the download page select `custom > $0`
- [Linux Mint](https://www.linuxmint.com/download.php) - Comes with either KDE, MATE or Cinnamon desktop. All three are fairly traditional and should make for an easy transition from windows.

Once you have picked a distribution, download the `.iso` file for that distribution.

While that is downloading download [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads) and install it. To create a new virtual machine click new and type in the name of the distro you chose. It should automatically choose the proper settings for `Type` and `Version`. If it doesn't and you selected from the above list you can automatically pick `Type: Linux` and if you picked Debian `Version: Debian(x64)` and if you picked anything else `Version: Ubuntu(x64)`

Click `next`

For memory size, assign as much memory as you can afford to the virtual machine. If you have 16 Gb of RAM, 12 Gb is a good amount to assign to the virtual machine. If you have less than that make sure you leave at least 2 Gb for the host operating system. Eclipse will use at least 6 Gb of RAM to build the project so if you cannot spare that much (8 Gb of system memory) you should try and upgrade.

Click `next`

Select `Create a virtual hard disk now` and click `Create`

Select VMDK and click `Next`

Select `Dynamically allocated` **NOTE:** if you fill up the initially allocated space on the VM the virtual disk will expand to fit all of the data but cannot be reclaimed by the host (Windows) without deleting the virtual machine. If this is not acceptable, select `Fixed Size` but be aware that the disk image will be locked to whatever size you choose in the next step.

For storage anything between 15 Gb > 30 Gb should be plenty. Keep in mind if you chose `Fixed Size` that once you fill up the virtual drive that you cannot expand it. Consider adding more storage if you went this route.

Click `Create`.

Wait until your `.iso` file has finished downloading. Once it is finished, click on your virtual machine in the side menu and click `Settings` from the top bar. In the settings menu, select `System` and make sure the allocated RAM is greater than 6 Gb. Go to the `Processor` tab. Anything less than 3 cores will cause extremely long build times. If you have an 8 core CPU I strongly reccommend allocating 6 cores to the virtual machine. If you have a four core, I would allocate 3. If you have a 2 core I would try and get an upgrade.

Navigate to the `Display` tab and select the maximum 128 MB of video memory click `Ok`.

Select the virtual machine from the side menu and hit `Start`. When the OS first boots up you will need to select the disk image (`.iso` file) from the prompt.

The next steps will be different for every distribution. Most will have an option for selecting `Install <DISTRO>` or `Try <DISTRO>`, make sure to hit Install.

When it comes to a menu that looks like the following:

```
...
Erase Disk and install <DISTRO>
Install <DISTRO> alongside X
...
```
Make sure to select `Erase Disk and Install`. This will only wipe and format the **Virtual Disk** and will not touch your files.

Most of the install is fairly straight forward, if you are ever unsure the default values are usually fine and it is impossible to damage your windows filesystem so if something goes wrong you can just delete the machine.

When the installation finishes it might say something like "Remove installation media and hit ENTER". If you see this just hit enter, the installation media refers to a step when installing linux on a physical machine.

___

## Git overview

> Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency

Git allows developers to safely collaborate on software by providing features like branching, rollbacks and more. Here is a quick overview of how git can (and sometimes how it should) be used.

This is meant to be a functional overview of the absolute basics. For a proper cheat sheet, see the pdf document in this repo. For a proper tutorial see [this link](https://git-scm.com/docs/gittutorial) or view the official [git documentation](https://git-scm.com/docs)


### Branching

A git branch simply means the state of the software with a specific name. As development happens on that software the branch "Grows". Every git repo has one branch by default named `master`. Traditionally that branch is considered the most stable and is often used as a release branch for advanced software. After that developers are responsible for creating more branches. Branches can be created with the git command

`git checkout -b <BRANCHNAME>`

To switch to a branch that already exists, omit the `-b` tag

`git checkout <BRANCHNAME>`

To check what git branch you are on, along with some more information, use the command

`git status`

The purpose of creating extra branches is to keep code seperated. For example if there are end users that rely on the stability of your `master` branch but you want to make changes that can't be considered stable until tested you might consider creating a `develop` branch and maybe even a `testing` branch.

```
         /----Develop
    /---------Testing
--------------Master
```

In the hypothetical situation above, once the developers are satisfied with their work, they would do something called a **Pull Request** and ask for a merge with testing. The admins of the git repository would review the code on the Develop branch and merge it into Testing. After a merge takes place, the admin can decide to destroy develop or keep it. If it is destroyed Testing now contains the changes from Develop and develop no longer exists. If it is not destroyed, Testing is altered to contain the code from develop but Develop remains untouched.

The testing team would make sure that the code was stable before performing the same action to merge it with Master.

Different branches can also be used to work on seperate parts of the software.

```
    /----------Installation_Scripts
----------Master
   \--------GUI_Design

```

In this situation, the GUI and Installation Scripts are seperate and can be worked on seperately and merged at different times. If the developers make a change in installation scripts that conflicts with a change in GUI_Design, the maintainers of the repository would have to perform a code audit to resolve the conflicts.

### Commits

Commits are very useful for incremental development. At any point the code can be reverted to where it was at a previous commit. They also provide a sort of roadmap for the softwares development in the form of commit messages.

When you have made a significant change to your code and would like to commit it before making more changes you can execute   

`git add <FILES>`

Entire directories, wildcards and specific files are supported, for example:


`git add *.java ./styles/ ./html/index.html`

It is important to note that if more than one developer are working on the same branch and have made changes at the same time, it is important for those developers to stash their changes and update their project before committing in order to avoid conflicting code.

```bash
# Stash your changes locally
git stash

# Update your code so that it is consitent with the newest commit for your branch
git pull

# Re-apply your changes to the newly updated code
git stash apply
```

These are the files that are to be "staged" for commit. you can use the `--all` flag to stage all changes.

Even though the files are staged they are still not ready to be added to the server. For that, commit them with a message like so:

`git commit -m "Made some excellent changes"`

This will assign your changes a 'hash' which is just a hexadecimal? number that identifies your commit.

Now you can push the commit to the server on branch that you are working on.

`git push origin <BRANCH>`

___

## Basic Installation - Eclipse and Ellucian Software

This is how it **should** go.

First, you need to install Java. To install Java 7 see the "Installing Java 7" section of this document.

We will also install Java 8 as it is required for the installation of eclipse.

Simply run 

`sudo apt-get install openjdk-8-jdk openjdk-8-jre`

once it is finished installing run 

`sudo update-alternatives --config java`

and choose the option that looks like openjdk8.

### Installing Eclispe

Once java is installed, download the [Eclipse Oxygen](https://www.eclipse.org/oxygen/) tar.gz file

Navigate to the location of the download in a terminal then run the following command to extract Eclipse

`sudo tar -xf eclipse-*.tar.gz`

Then you can cd into the installer directory

From there run the installer

`./eclipse-installer`

When prompted, make sure you install **Eclipse IDE for Jave EE developers**

When eclipse finishes, run `sudo update-alternatives --config java` and select java 7 from the list. If there is no option for java 7, see the "installing java 7" section of this document.

### Install New Software - Eclipse

When the installation finishes, install the extra software go `Help > Install new Software`. Click `Add...` and enter the following,

Name: Sapphire

URL: `http://download.eclipse.org/sapphire/8.3/repository`.

Click `Ok` and when the Sapphire repository appears in the box below click `Select All` then `Next`. Wait for it to install and when it says `Do you want to restart eclipse?` Make sure to click **No**. We do not know why but the Ellucian documentation is very specific about this.

Do the same thing for the morphis repository,

`Help > Install New Software`

`Add`

`NAME: bannerpages`

`URL: https://artifactory-pro.elluciancloud.com/artifactory/eclipse-p2-local/bannerpages-mars`

`Select All > Finish`

This will probably prompt you for a Ellucian artifactory login. Enter your login information (if you have it) and hit `Ok`

Once this download finishes you can click `Restart Now`.

### Installing Tomcat

We will install Tomcat 7.0.

From [Tomcat](https://tomcat.apache.org/download-70.cgi) download the tar.gz file and navigate to the `Downloads` directory.

From there create the directory where this version of Tomcat will live

`sudo mkdir /usr/share/tomcat7.0.xx` where `xx` is the last bit of the version number downloaded.

Then unzip the tar file into that directory

`sudo tar -xf apache-tomcat-7.0.xx.tar.gz -C /usr/share/tomcat7.0.xx`

This would work as is but we should also simlink a directory `tomcat` so that if we choose a different version in the future it won't break ~~anything~~ as many things.

`sudo ln -sf /usr/share/tomcat7.0.xx/ /usr/share/tomcat/`


### Configuring the Eclipse GUI

We will first open the Foundations perspective.

`Perspective > Open Perspective > Foundations`

Note: To reset the perspective after making changes `Perspective > Reset Perspective`

The servers window will not be displayed by default. To display it `Windows > Show View > Other > Server > Servers` I also like to drag the server window from the right panel to the bottom panel but that is just personal preference

### Foundations Generator Settings

Ellucian Banner Admin Pages have specific settings for the Generator. To enter them select `Windows > Preferences > Foundations > Generator`

Select the `Profile` drop down and choose the `Custom` profile.

Enter the corresponding setting for every setting from the table below.

| Setting | Value |
|---------|-------|
| Task Base class | net.hedtech.general.common.forms.BaseTask |
| TaskPart base class | morphis.common.forms.baseForm.DefaultTaskPart |
| Model base class | morphis.foundations.core.appsupportlib.model.FormModel |
| DataSet default class | org.jdesktop.databuffer.DataSet |
| SBO Adapter default class | morphis.foundations.flavors.forms.appsupportlib.model.SimpleBusinessObject |
| DBO Adapter default class | morphis.foundations.flavors.forms.appdatalayer.data.BaseRowAdapter |
| Task controller base class | net.hedtech.general.common.forms.controller.DefaultFormController |
| Block controller base class | net.hedtech.general.common.forms.controller.DefaultBlockController |
| Menu controller base class | morphis.foundations.core.appsupportlib.runtime.control.AbstractMenuController |
| Support code base class | morphis.common.forms.baseForm.services.DefaultSupportCodeObject |
| Report task base class  | morphis.common.forms.reportPf.DefaultPFTask |
| Report model base class | morphis.common.forms.reportPf.model.DefaultPFFormModel |
| Report task controller base class | morphis.common.forms.reportPf.controller.DefaultPFFormController |
| Report block controller base class | morphis.common.forms.reportPf.controller.DefaultPFBlockController |
| Report support code base class  | morphis.common.forms.reportPf.services.PFServices |

Once they are all set, hit `Apply and close`

Go `Window > Preferences > Foundations > Configuration`

Beside Task Contexts hit `New` and add `net.hedtech` to the list of Task Contexts

Change the `Default base task` to `BASE.Task.config` .

Click `Apply and Close > Yes`

Go `Window > Preferences > Validation` and scroll to the very bottom of the list.

From the **Columns** labelled Manual and Build, uncheck the tick boxes for the **Rows** labelled `XML Schema Validator`, `XML Validator` and `XSL Validator` and then hit `Apply`

We are done with Eclipse for now and so we can close it and open a terminal window. We need to create a `morphis_config.ini` file. We will put it in the same directory as our Eclipse installation.

`touch ~/eclipse/jee-oxygen/eclipse/morphis_config.ini`

If that command fails it is likely because your eclipse is installed to a different directory. It does not really matter where you put your `morphis_config.ini` so long as you know where to find it.

Once you have done that open it in a text editor

`gedit ~/eclipse/jee-oxygen/eclipse/morphis_config.ini`

You may need to install gedit

`sudo apt-get install gedit`

When it opens, paste the following text into the file and hit `ctr + s` to save.

```
morphis.foundations.plugin.core/blockControllerBaseClass=net.hedtech.general.common.forms.controller.DefaultBlockController
morphis.foundations.plugin.core/custom_defaultBlockControllerBaseClass=net.hedtech.general.common.forms.controller.DefaultBlockController
morphis.foundations.plugin.core/custom_defaultDataSetClass=org.jdesktop.databuffer.DataSet
morphis.foundations.plugin.core/custom_defaultFormControllerBaseClass=net.hedtech.general.common.forms.controller.DefaultFormController
morphis.foundations.plugin.core/custom_defaultMenuControllerBaseClass=morphis.foundations.core.appsupportlib.runtime.control.AbstractMenuController
morphis.foundations.plugin.core/custom_defaultRowBaseClass=morphis.foundations.flavors.forms.appdatalayer.data.BaseRowAdapter
morphis.foundations.plugin.core/custom_defaultSimpleBusinessObjectBaseClass=morphis.foundations.flavors.forms.appsupportlib.model.SimpleBusinessObject
morphis.foundations.plugin.core/custom_defaultSupportCodeBaseClass=morphis.common.forms.baseForm.services.DefaultSupportCodeObject
morphis.foundations.plugin.core/custom_modelBaseClass=morphis.foundations.core.appsupportlib.model.FormModel
morphis.foundations.plugin.core/custom_reportBlockControllerBaseClass=morphis.common.forms.reportPf.controller.DefaultPFBlockController
morphis.foundations.plugin.core/custom_reportFormControllerBaseClass=morphis.common.forms.reportPf.controller.DefaultPFFormController
morphis.foundations.plugin.core/custom_reportModelBaseClass=morphis.common.forms.reportPf.model.DefaultPFFormModel
morphis.foundations.plugin.core/custom_reportServicesBaseClass=morphis.common.forms.reportPf.services.PFServices
morphis.foundations.plugin.core/custom_reportTaskBaseClass=morphis.common.forms.reportPf.DefaultPFTask
morphis.foundations.plugin.core/custom_taskBaseClass=net.hedtech.general.common.forms.BaseTask
morphis.foundations.plugin.core/custom_taskPartBaseClass=morphis.common.forms.baseForm.DefaultTaskPart
morphis.foundations.plugin.core/defaultBaseTaskConfiguration=BASE.Task.Config
morphis.foundations.plugin.core/defaultRowBaseClass=morphis.foundations.flavors.forms.appdatalayer.data.BaseRowAdapter
morphis.foundations.plugin.core/defaultSimpleBusinessObjectBaseClass=morphis.foundations.flavors.forms.appsupportlib.model.SimpleBusinessObject
morphis.foundations.plugin.core/eclipse.preferences.version=1
morphis.foundations.plugin.core/formControllerBaseClass=net.hedtech.general.common.forms.controller.DefaultFormController
morphis.foundations.plugin.core/generatorProfile=Custom
morphis.foundations.plugin.core/menuControllerBaseClass=morphis.foundations.core.appsupportlib.runtime.control.AbstractMenuController
morphis.foundations.plugin.core/modelBaseClass=morphis.foundations.core.appsupportlib.model.FormModel
morphis.foundations.plugin.core/taskBaseClass=net.hedtech.general.common.forms.BaseTask
morphis.foundations.plugin.core/taskContexts=net.hedtech;morphis
org.eclipse.wst.validation/org.eclipse.wst.xsl.core.xsl=TF02
vals/org.eclipse.wst.xml.core.xml/global=FF03
vals/org.eclipse.wst.xsd.core.xsd/global=FF02162org.eclipse.wst.xsd.core.internal.validation.eclipse.Validator
vals/org.eclipse.wst.xsl.core.xsl/global=FF02

```

Once you have saved, exit gedit. We now need to tell eclipse to access the `morphis_config.ini` file on startup. For that we will open a terminal and use the following command to open the eclipse.ini file in gedit. `gedit ~/eclipse/jee-oxygen/eclipse/configuration/config.ini`. with that open we can simply add the following line at the bottom and hit `ctr + s` to save.

`eclipse.pluginCustomization=<path>/morphis_eclipse.ini`

Where `<path>` is the absolute path to the morphis_eclipse.ini file.

___

## Git clone commands

The TRU server requires that you generate an SSH key in order to access the GIT server. To do so, enter the following command in your terminal

`ssh-keygen -t rsa -C "youremail@example.com"`

The -t flag specifies the type of algorithm key to generate (the tru git server uses rsa), and the -C flag is simply a comment that can be used to identify the key later on.

You can make a password if you want but you will have to enter it once for every repository that you wish to clone which can get fairly tedious.

This key needs to be sent to Rick Gunnyon so that he can add it to the repository and grant access. The key, by default is stored at `~/.ssh/id_rsa.pub`

Copy the key to your desktop and rename it something like `firstname-lastname.pub`

The command to do this will look like so:

`sudo cp ~/.ssh/id_rsa.pub ~/Desktop/firstname-lastname.pub`

Next we will set up the directory for storing our repositories.	

Create the `~/admin_page/dev90/` directory. Run `mkdir ~/admin_page && cd ~/admin_page && mkdir dev90 && cd dev90`

Following are the required repositories necessary to build the project:

It is recommended to turn this into a script, from the `~/admin_page/dev90/` directory

Make sure `git` is installed (for debian/Ubuntu `sudo apt-get install git`)

```
touch clone_repos.sh
nano clone_repos.sh
```
Then paste the following text

```bash
#!/usr/bin/env bash

git clone git@gitvc.tru.ca:banner/pages/alumni
git clone git@gitvc.tru.ca:banner/pages/arsys
git clone git@gitvc.tru.ca:banner/pages/base
git clone git@gitvc.tru.ca:banner/pages/bdr
git clone git@gitvc.tru.ca:banner/pages/bfr-student
git clone git@gitvc.tru.ca:banner/pages/build
git clone git@gitvc.tru.ca:banner/pages/common
git clone git@gitvc.tru.ca:banner/pages/extsol
git clone git@gitvc.tru.ca:banner/pages/finance
git clone git@gitvc.tru.ca:banner/pages/general
git clone git@gitvc.tru.ca:banner/pages/payroll
git clone git@gitvc.tru.ca:banner/pages/positioncontrol
git clone git@gitvc.tru.ca:banner/pages/reports
git clone git@gitvc.tru.ca:banner/pages/student
git clone git@gitvc.tru.ca:banner/pages/tru-admissions
git clone git@gitvc.tru.ca:banner/pages/tru-alumni
git clone git@gitvc.tru.ca:banner/pages/tru-arsys
git clone git@gitvc.tru.ca:banner/pages/tru-bdr
git clone git@gitvc.tru.ca:banner/pages/tru-bfr.student
git clone git@gitvc.tru.ca:banner/pages/tru-common-libraries
git clone git@gitvc.tru.ca:banner/pages/tru-course
git clone git@gitvc.tru.ca:banner/pages/tru-degree
git clone git@gitvc.tru.ca:banner/pages/tru-document-management
git clone git@gitvc.tru.ca:banner/pages/tru-extsol
git clone git@gitvc.tru.ca:banner/pages/tru-finance
git clone git@gitvc.tru.ca:banner/pages/tru-finance-common
git clone git@gitvc.tru.ca:banner/pages/tru-general
git clone git@gitvc.tru.ca:banner/pages/tru-general-common
git clone git@gitvc.tru.ca:banner/pages/tru-learner
git clone git@gitvc.tru.ca:banner/pages/tru-payroll
git clone git@gitvc.tru.ca:banner/pages/tru-positioncontrol
git clone git@gitvc.tru.ca:banner/pages/tru-student
git clone git@gitvc.tru.ca:banner/pages/tru-student-common
git clone git@gitvc.tru.ca:banner/pages/tru-validation
git clone git@gitvc.tru.ca:banner/pages/workspace

```
Next enter `sudo chmod +x clone_repos.sh`

Finally run the script `./clone_repos.sh`

If you set up a password for your git credentials you will need to enter it once for every git clone command.

Once the repos are cloned, see "Removing Rick's Config Information" to make sure everything is in order. Then there are a couple of changes that need to be made in the `~/admin_page/dev90/base/net.hedtech.banner/pom.xml`
___
## Importing projects into Eclipse

Before we import the projects into eclipse we will need to increase the amount of memory that eclipse can use. The recommended values are `Min = 1Gb Max = 4Gb` If your system does not have enough RAM to accomodate these requirements, change the values to `Min = 512Mb Max = 2Gb`.

To increase the RAM allocation (heap size), open a terminal and run `gedit ~/eclipse/jee-oxygen/eclipse/eclipse.ini` and look for these two lines:

```
-Xms256m
-Xmx1024m
```

If you can afford 4Gb of RAM, change the values to

```
-Xms1g
-Xmx4g
```

Otherwise change the values to

```
-Xms512m
-Xmx2g
```

Note that you **will not** be able to build this project with less than 2Gb

`ctrl + s` to save and exit gedit.

Next create the `~/admin_page/dev90workspace/` directory. Run `cd ~/admin_page/ && mkdir dev90workspace`

Open eclipse and this time choose the `/<USERNAME>/admin_page/dev90workspace` as your workspace instead of the default.

We have to increase the memory size from inside eclipse as well. `Window > Preferences > Java > Installed JRE's` then select the JRE version you are running and click Edit.

**IMPORTANT: IF YOU CHANGE THE JRE IN THE FUTURE (YOU PROBABLY WILL) YOU HAVE TO CHANGE THIS VALUE AGAIN** see <u>installing Java 7</u> for more info

In the box that says `Default VM Arguments` add the options above so that the box reads `-Xms1g -Xms4g` or `-Xms512m -Xms2g` depending on which size options you added to `eclipse.ini`

From Eclipse `File > Import > General > Existing Projects into Workspace` then select the `/home/user/admin_page/dev90` as the root directory and choose the following packages to import:

- admissions
- alumni
- arsys
- bdr
- bfr.student
- common-libraries
- course
- degree
- document-management
- extsol
- finance
- finance-common
- general
- general-common
- learner
- payroll
- positioncontrol
- reports
- student
- student-common
- tru-admissions
- tru-alumni
- tru-arsys
- tru-bdr
- tru-bdr.student
- tru-common-libraries
- tru-course
- tru-degree
- tru-document-management
- tru-extsol
- tru-finance
- tru-general
- tru-general-common
- tru-learner
- tru-payroll
- tru-positioncontrol
- tru-student
- tru-student-common
- tru-validation
- validation
- Webapp.HR.Services
  - There will be more than one with this name, import all of them
- Webapp.HR.Workspace
- Webapp.Student.Services

**If you are missing any of the above projects it is likely due to one of the git commands failing. Re-run the script from `~/admin_page/dev90/` and keep an eye on the output for errors**

___

## Removing Rick's config information

Some of the git repos contain information regarding Rick's personal machine. This will cause errors until they are changed in the git repository. Verify that you have the correct config by running this command from the `~/admin_page/dev90/` directory.

`cat ./common/common-libraries/.classpath | grep xmltooling | grep rgunnyon`

If the output contains your user information then the repo has been updated. If you see anything that resembles `rgunnyon` you have to change the config in Eclipse to match the path to your `xmltooling.jar`.

Open eclipse and right click on `common-libraries` then go `Properties > Java Build Path > Libraries` click on `xmltooling` and click `edit`. Then browse to `~/.m2/repository/org/opensaml/xmltooling/<VERSION_NUMBER>/xmltooling<VERSION_NUMBER>.jar`

Note: You will have to do the same thing for `tru-common-libraries`

Then do a full maven update. Select any project, right click on it in the project explorer and go `Maven > Update Project` then check `Select All` and `Force Update of Snapshots/Releases` then `Finish`

___

## Javascript Version issue

Many errors were caused by a java version compatibility issue. There are two fixes available.

1. Modify the build path of every single project to include Java 8 (Rob has had success with this)
2. Install Java 7 and set it as default

I will cover method 2 because it is what I did and it worked. Note that this strategy is fairly regressive and might not be the best solution.

### Installing Java 7

The github user chrishantha has an excellent script for installing java from extracted zip files. The script can be cloned via the command

`git clone https://github.com/chrishantha/install-java`

The script has one dependency and that is the `unzip` program. Make sure it is installed with

`sudo apt-get install unzip`

Once unzip is installed, cd into the install-java directory. `cd install-java` then open up a web browser.

Navigate to [oracle's archive download page](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html) and download the Linux x64 version of java 7. Make sure it is the **jdk-7u80-linux-x64.tar.gz** file.

Go back to your terminal and execute the `./install-java.sh` script with the following options.

- -f <JAVA.tar.gz>
- -p <JAVA INSTALLATION DIRECTORY>

The only mandatory option is the -f specification of the tar.gz file. The command will look like this if you don't want to specify a custom installation location

`sudo ./install-java.sh -f ~/Downloads/jdk-7u80-linux-x64.tar.gz`

### Set Java 7 as system default

Once the script has finished JDK 7 should be installed on your system but it **will not be running**. For that you need to enter the following command.

`sudo update-alternatives --config java`

That command will open a CLI program to help you select the proper version of Java. From the list of installed JRE's find the one that has 7 in the name, enter the corresponding number and hit `Enter`.

Be aware that Java 7 is now the default JRE on your system and that comes with all the bugs and security flaws of unsupported software. If another program that depends on Java fails, try re-running `sudo update-alternatives --config java` again and selecting a more recent version.

### Configure your projects to use Java 7

All (most?) of the projects we cloned earlier use Java 7 as the default. This means they will look through the list of installed JRE's and choose to run on Java 7 if it is available. To make Java 7 visible to these projects we will do the following.

Open eclipse and go `Window > Preferences > Java > Installed JRE's` and click `Add` choose `Standard VM` and click `Next`. From there browse to `/usr/lib/JVM/jdk1.7.x/` and hit `Ok > Finish > Apply and Close`

Then you need to go to `Window > Preferences > Java > Compiler` and change the compiler version to 1.7. Then hit `Apply and close`

After changing that setting the project will probably rebuild itself but it is usually best to do another maven update.

Select any project, right click on it in the project explorer and go `Maven > Update Project` then check `Select All` and `Force Update of Snapshots/Releases` then `Finish`

___

## Common Libraries dependancy Issue

If all or most of your projects are failing after completing all steps above, it is possible that since most of them are depending on common-libraries and tru-common-libraries, and the build fails before getting to them, the build will be stuck in a catch-22.

The first thing to try is right clicking on `common-libraries` in the project explorer and going `Maven > Update Project` then select both `common-libraries` and `tru-common-libraries`. Check the Force Updates of Snapshots/Releases and hit finish.

**If that works <u>DO NOT DO THE NEXT STEP</u>**

The other option is to open a terminal and `cd` into `~/admin_page/dev90/common/common-libraries` and run

`mvn clean -U install`

This will build just that project and is more likely to be successful than running it from Eclipse but could cause a maven versioning issue.

___

## Cleaning dependencies

Errors that contain something like "...repository is cached locally and will not be updated..." can sometimes be fixed by deleting some files and downloading the dependencies again.

The command to find and delete the proper files is

`find ~/.m2 -name "*.lastUpdated" -exec grep -q "Could not transfer" {} \; -print -exec rm {} \;`

  and can be run from anywhere. After that command, perform a Maven Update on all projects

  Select any project, right click on it in the project explorer and go `Maven > Update Project` then check `Select All` and `Force Update of Snapshots/Releases` then `Finish`

### import morphis.foundations.flavors.forms.forms.appdatalayer.data.BaseRowAdapter; cannot be resolved

There is an extra `forms` in the import path. Just delete it.

## Add pre existing key to new install

This is for if you want to add a pre-existing public key to a new operating system because you are changing computers. Generating a new key works too but might begin to bug rick eventually.

It is actually quite simple, the key files just needs to be copied to the `~/.ssh/` directory. The key files will be named `id_rsa` and `id_rsa.pub` if they have not been renamed and will be in the `~/.ssh/` directory of the old machine. Copy them via email or usb stick and send them to your new machine, pasting them into the `~/.ssh/` directory.

Note that id_rsa ~~is~~ might be a private key and probably shouldn't be shared over email. (Disclaimed :) )
___

# Development
___

## Creating a form with a keyblock

To begin, right click on the parent project and select `New > Other > Task`

Leave the source folder on default and change the `Package` to `net.hedtech.banner.tru.<project_name>` where `<project_name>` is the parent project (finance, student, etc...)

Set `name` to the name of the form (SPRIDEN, TZARCPT, etc...)

Base Task should automatically be set to `BASE.Task.Config` If it is not, you might need to edit some preferences. Close the dialogue and go `Window > Preferences > Foundations > Configuration` and set Default Base task to `BASE.Task.Config`

Click next. In the list, select `Task with keyblock` and hit next.

If you have already connected to the Oracle database you should be able to select the connection that you created and choose a database. For this you will need to know where the tables in your form reside and what their names are.

You can only add one table here (to start with) and so make sure it is the one that has all (or at least the most) elements that your keyblock will contain.

Select your connection from the `Name` field. Select the Database from the `Database` field. Select the schema from the dropdown labeled `Schema` NOTE: `?` is treated as a wildcard. Select your table from the `Table/View` selector and choose the columns you want to add to your form. These columns are not just for the keyblock so make sure to add all that will be represented by the form. Hit next.

Add a `Title` that will be displayed at the top of the form. Leave width and height alone.

### Selecting between tabular and form

Make sure you choose right here as there is no way to change this selection without deleting the whole form and starting again.

**Tabular** - The tabular block type is most useful when there is more than one row expected as a result. For example if you wanted to `Select all student_id, studen_name from students where age > 20` and wanted those results displayed row by row, you would select tabular

**Form** - The Form block type is used when you are working with a set of results for a single element. For example `Select all first_name, last_name, pidm from staff where name = "Alan Schaver"`

Choose either **Form** or **Tabular** and hit next.

From this screen you will choose the elements from this table that are to be included in the keyblock. Click `Choose` and pick from the list. `> Ok`

Click `Finish`

### Adding elements from other tables

There is more than one way that this can be accomplished. The two main methods are **Virtual** and **Physical** fields.

**Virtual** - These fields are populated via a query that is triggered after the main query. They cannot be used to physically change data in the database, only to view data. *THIS MIGHT BE WRONG ->* Using virtual fields could incur a performance penalty as a sub-query has to be run for each result in the main query. (No performance hit for **Form** block type)

**Physical** - These fields are populated with the main query and are still a mystery to me.

#### Adding elements to a Virtual Field - Credit : Rob

For the sake of explanation substitute **FormName** with the name of your form and **TableName** with the name of the table you included in the <u>keyblock</u> creation dialog. Note that this will necessarily be different from the **NewTableName** even though some methods will look like they don't belong.

1. Open the `FormNameStruct.xml` file and add the following line where NewField is the name of the field in the database (i.e. `SPRIDEN_PIDM`). Make sure this line is inserted in the order you would have it appear in the form.

```XML
<Item CanvasName="MAIN_CANVAS" Id="NewField" Type="TextItem"/>
```
2. Save all and open the `PanelMainCanvas.xvc` and add the following code, again in the same relative order as you did in `FormNameStruct.xml` . If the other elements are wrapped in `<gridcell>` tags do the same for this one, if not they are optional.

```xml


```

___

## Extending a page (Clone a page and change something)

- Create a page extension -> Page 19, <i>Extending Your Administrative Application</i>
